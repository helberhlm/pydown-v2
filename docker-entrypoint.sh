#!/bin/bash

CONFIG_FILE=".env"
path_number=1
set -e

cat >"$CONFIG_FILE" <<EOT
# ######################################################################################
#  ____        ____                                 ____                               #
# |  _ \ _   _|  _ \  _____      ___ __      __   _|___ \                              #
# | |_) | | | | | | |/ _ \ \ /\ / / '_ \ ____\ \ / / __) |                             #
# |  __/| |_| | |_| | (_) \ V  V /| | | |_____\ V / / __/                              #
# |_|    \__, |____/ \___/ \_/\_/ |_| |_|      \_/ |_____|                             #
#        |___/                                                                         #
#                                                                                      #
# Dependências necessárias:                                                            #
#   - pip3 install -r requirements.txt                                                 #
#                                                                                      #
#   - Trabalho futuro: OMDB (API online para obter informações extras das mídias)      #
#       python -m pip install omdbapi                                                  #
# ######################################################################################


# Deve-se obter a api_id e api_hash na página de configuração do Telegram
# https://my.telegram.org/auth?to=apps
EOT

# Transform long options to short ones
for arg in "$@"; do
  shift
  case "$arg" in
    "--api_id")          set -- "$@" "-i" ;;
    "--api_hash")        set -- "$@" "-h" ;;
    "--my_username")     set -- "$@" "-u" ;;
    "--bot_name")        set -- "$@" "-b" ;;
    "--bot_token")       set -- "$@" "-t" ;;
    "--session_name")    set -- "$@" "-s" ;;
    "--debug")           set -- "$@" "-d" ;;
    "--user_downloader") set -- "$@" "-w" ;;
    "--allowed_users")   set -- "$@" "-g" ;;
    "--path")            set -- "$@" "-p" ;;
    *)                   set -- "$@" "$arg"
  esac
done

while getopts ":i:h:u:b:t:s:d:w:g:p:" opt; do
    case $opt in
        i)
        echo -n "API ID -> "
        echo $OPTARG
        echo "api_id=$OPTARG" >> "$CONFIG_FILE" 
        # exit 1
        ;;
        h)
        echo -n "API Hash -> "
        echo $OPTARG
        echo "api_hash=$OPTARG" >> "$CONFIG_FILE" 
        # exit 1
        ;;
        u)
        echo -n "Username -> "
        echo $OPTARG
        echo "my_username=$OPTARG" >> "$CONFIG_FILE" 
        # exit 1
        ;;
        b)
        echo -n "Bot Name -> "
        echo $OPTARG
        echo "bot_name=$OPTARG" >> "$CONFIG_FILE" 
        # exit 1
        ;;
        t)
        echo -n "Bot Token -> "
        echo $OPTARG
        echo "bot_token=$OPTARG" >> "$CONFIG_FILE" 
        # exit 1
        ;;
        s)
        echo -n "Session Name -> "
        echo $OPTARG
        echo "session_name=$OPTARG" >> "$CONFIG_FILE" 
        # exit 1
        ;;
        d)
        echo -n "Debug -> "
        echo $OPTARG
        echo "debug=$OPTARG" >> "$CONFIG_FILE" 
        # exit 1
        ;;
        w)
        echo -n "User Downloader -> "
        echo $OPTARG
        echo "user_downloader=$OPTARG" >> "$CONFIG_FILE" 
        # exit 1
        ;;
        g)
        echo -n "Users Allowed -> "
        echo $OPTARG
        echo "allowed_users=$OPTARG" >> "$CONFIG_FILE" 
        # exit 1
        ;;
        p)
        echo -n "Path $path_number -> "
        echo $OPTARG
        echo "path_$path_number=$OPTARG" >> "$CONFIG_FILE"
        path_number=$(($path_number+1))
        # exit 1
        ;;
    esac
done

cp $CONFIG_FILE PyDownBot/
cp $CONFIG_FILE PyDownUser/
exec python /usr/src/app/pydown.py