from pyrogram import Client
from pyrogram import Filters, MessageHandler, CallbackQueryHandler
from pyrogram import InlineKeyboardMarkup, InlineKeyboardButton
from dotenv import load_dotenv
import math
import sys, os


class PyDownBot:

    # Env Variables
    DEBUG = "False"
    api_id    = "123456"
    api_hash  = "abcdef"
    bot_name = "myBot"
    bot_token = "abc:123"
    user_downloader="myUserDownloader"
    allowed_users = "me"
    allowed_media_types = ["document", "video", "photo", "audio", "voice"]
    all_media_types = ['message', 'messageentity', 'photo', 'thumbnail', 'audio', 'document', 'animation', 'video', 'voice', 'videonote', 'contact', 'location', 'venue', 'sticker', 'game', 'webpage', 'poll', 'polloption']
    pathList = {}
    inlinePathsKeyboardButton = []

    # Internal constants
    CANCEL_CALLBACK = "cancel"
    CANCEL_DW_CALLBACK = "cancel_dw"
    def __init__(self, pyDownApp):
        self.pyDownApp = pyDownApp
        self.loadConfig()
        self.bot = Client(
            self.bot_name, 
            bot_token=self.bot_token,
            api_id=self.api_id,
            api_hash=self.api_hash,
            workdir="/usr/src/app/store/")
        self.initBotHandlers()

    def initBotHandlers(self):
        start_handler = MessageHandler(self.start_bot_command, filters=Filters.command("start"))
        self.bot.add_handler(start_handler)
        allowed_messages_handler = MessageHandler(self.newMessageAllowed, filters=Filters.chat(self.allowed_users))
        self.bot.add_handler(allowed_messages_handler)
        callback_handler = CallbackQueryHandler(self.on_callback)
        self.bot.add_handler(callback_handler)

    def loadConfig(self):
        print("Lendo configurações...")
        ## Lê as configurações a partir do arquivo .env
        load_dotenv()
        self.api_id          = os.getenv('api_id', default=self.api_id)
        self.api_hash        = os.getenv('api_hash', default=self.api_hash)
        self.bot_token       = os.getenv('bot_token', default=self.bot_token)
        self.bot_name        = os.getenv('bot_name', default=self.bot_name)
        self.user_downloader = os.getenv('user_downloader', default=self.user_downloader)
        self.DEBUG           = os.getenv('debug', default=self.DEBUG).lower() == "true"
        self.allowed_users   = os.getenv('allowed_users', default=self.allowed_users).split(',')
    
        # get path list in .env file
        i = 1
        while(os.getenv('path_'+str(i))):
            path = os.getenv('path_'+str(i))
            pathName = path.split(':')[0]
            pathDir = path.split(':')[1]
            self.pathList[pathName] = pathDir
            i = i + 1
        self.inlinePathsKeyboardButton = self.generateInlineKeyboard(self.pathList, 2)

    def generateInlineKeyboard(self, pathList, itemsPerLine=None):
        result = []
        # add all paths to Inline Keyboard list
        for key, value in pathList.items():
            result.append(
                InlineKeyboardButton(key, bytes(key, 'utf-8'))
            )
        # split keyboard buttons in per line limit
        if(itemsPerLine):
            x = 0
            newResult = []
            while( (x+itemsPerLine) < len(result) ):
                newResult.append(result[x:(x+itemsPerLine)])
                x = x + itemsPerLine
            newResult.append(result[x:len(result)])
            result = newResult
        return result

    def printd(self, *args, sep=" ", end="\n", flush = True):
        if(self.DEBUG):
            print(*args, sep = sep, end = end, flush = flush)

    def start_bot_command(self, client, message):
        message.reply("Bem vindo! Envie-me um arquivo para começarmos.")

    def newMessageAllowed(self, client, message):
        if not message.media:
            message.reply_text(
                "{}, envie um arquivo para prosseguirmos.".format(message.from_user.first_name),
                quote=True
            )
            return

        self.printd("Message: ", message, "\n\n")

        try:
            mediaObject = self.getFileObject(self.getMediaMessage(message))
            fileSize = self.convert_size(mediaObject.file_size)
        except:
            fileSize = "Error on get size"

        message.reply_text(
            "**Nome do Arquivo:** {}\n".format(self.getFileNameOrCaption(message)) +
            "**Tamanho:** {}\n".format(fileSize) +
            "\n" +
            "**Baixa em:**",
            quote=True,
            reply_markup=InlineKeyboardMarkup(
                self.inlinePathsKeyboardButton
                + [[InlineKeyboardButton("<< Cancelar >>", bytes(self.CANCEL_CALLBACK, 'utf-8'))]]
            )
        )
        # getMediaType(message)

    def on_callback(self, client, callback_query):
        self.printd("Callback Query:", callback_query, "\n\n")

        if(callback_query.message.reply_to_message.media):
            self.parseCallbackMessageWithMedia(client, callback_query)
        else:
            client.answer_callback_query(
                callback_query.id, 
                "É necessário enviar/encaminhar uma mensagem com mídia"
            )

    def parseCallbackMessageWithMedia(self, client, callback_query):
        callback_data = callback_query.data
        self.printd("Callback Query: ", callback_query)
        
        if(callback_data == self.CANCEL_CALLBACK):
            self.cancelDownload(callback_query)
        elif(callback_data.split(":")[0] == self.CANCEL_DW_CALLBACK):
            self.cancelDownloadInProgress(callback_query)
        elif(callback_data in self.pathList):
            self.downloadFile(callback_query)
        else:
            client.answer_callback_query(callback_query.id, text="Comando não reconhecido!")

    def cancelDownload(self, callback_query):
        callback_query.edit_message_text(
            "**Cancelado**"
        )

    def cancelDownloadInProgress(self, callback_query):
        bot_file_id = callback_query.data.split(":")[1]
        self.pyDownApp.updateDownloadList(bot_file_id, ("cancel_request", True))
        callback_query.edit_message_text(
            "**Cancelando...**"
        )

    def downloadFile(self, callback_query):
        fileName = self.getFileNameOrCaption(callback_query.message)
        self.printd("Download file", fileName, "at", self.pathList[callback_query.data])

        # Id para identificação do arquivo baixado. Ex.: 132316732_90
        bot_file_id = str(callback_query.from_user.id) + "_" + str(callback_query.message.message_id)
        button_callback = self.CANCEL_DW_CALLBACK + ":" + bot_file_id

        self.pyDownApp.updateDownloadList(bot_file_id, 
            ("file_name", fileName),
            ("path_download", self.pathList[callback_query.data]+str(fileName))
        )
        callback_query.edit_message_text(
            "**Baixando...**",
            reply_markup=InlineKeyboardMarkup(
                [[InlineKeyboardButton("Cancelar", bytes(button_callback, 'utf-8'))]]
            )
        )
        # Se o user que for baixar o arquivo não for o mesmo da conversa do bot, então
        # encaminha a mídia e envia o comando para iniciar o download
        if(not callback_query.from_user.username == self.user_downloader or
            callback_query.from_user.id == self.user_downloader):
            fw_message = callback_query.message.reply_to_message.forward(self.user_downloader)
            fw_message.reply_text(
                "**Downloading:**",
                quote=True,
                reply_markup=InlineKeyboardMarkup(
                    [[InlineKeyboardButton("Cancelar", bytes(button_callback, 'utf-8'))]]
                )
            )

    def getFileNameOrCaption(self, message):
        messageWithMedia = self.getMediaMessage(message)
        if not messageWithMedia:
            return "No media file"
        elif hasattr(self.getFileObject(messageWithMedia, allowed_media_types=self.allowed_media_types), 'file_name'):
            return self.getFileObject(messageWithMedia, allowed_media_types=self.allowed_media_types).file_name
        elif messageWithMedia.caption:
            return messageWithMedia.caption
        elif hasattr(self.getFileObject(messageWithMedia, allowed_media_types=self.allowed_media_types), 'file_id'):
            return self.getFileObject(messageWithMedia, allowed_media_types=self.allowed_media_types).file_id
        else: return "Error on get media file"

    def getMediaMessage(self, message):
        if message.media:
            return message
        elif hasattr(message, 'reply_to_message') and hasattr(message.reply_to_message, 'media'):
            return message.reply_to_message
        else:
            return False

    def getFileObject(self, messageWithMedia, allowed_media_types=None):
        mediaType = self.getMediaType(messageWithMedia)
        if not mediaType or ( # Se não é um arquivo de mídia conhecido ou se não está na lista de tipos autorizados...
            allowed_media_types and not mediaType in allowed_media_types):
            return False
        # printd("### mediaType:", mediaType)
        # printd("### messageWithMedia:", messageWithMedia)
        # printd("### messageWithMedia[mediaType]:", messageWithMedia[mediaType])
        return messageWithMedia[mediaType]

    def getMediaType(self, messageWithMedia):    
        for k in self.all_media_types:
            try:
                if messageWithMedia[k]:
                    return k
            except:
                # self.printd('Erro getMediaType [', k, ']')
                pass
        return False

    def convert_size(self, size_bytes):
        if size_bytes == 0:
            return "0B"
        size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
        i = int(math.floor(math.log(size_bytes, 1024)))
        p = math.pow(1024, i)
        s = round(size_bytes / p, 2)
        return "%s %s" % (s, size_name[i])

    def onUpdateDownloadList(self, downloadList):
        self.printd(">>>", "Novidades...", downloadList)
        for key, value in downloadList.items():
            # if not "last_update" in value:
            #     downloadList[key]["last_update"] = ""
            if value["download_status"] == "queue":
                if not "last_update" in value or value["last_update"] != "queue":
                    self.pyDownApp.updateDownloadList(key, ("last_update", value["queue"]))
                    self.updateQueueDownload(key, value)
            elif value["download_status"] == "downloading":
                if not "last_update" in value or value["progress_count"] != value["last_update"]:
                    self.pyDownApp.updateDownloadList(key, ("last_update", value["progress_count"]))
                    self.updateProgressCount(key, value)
            elif value["download_status"] == "complete":
                if not "last_update" in value or value["last_update"] != "complete":
                    self.pyDownApp.updateDownloadList(key, ("last_update", "complete"))
                    self.updateCompleteDownload(key)
            elif value["download_status"] == "cancelled":
                if not "last_update" in value or value["last_update"] != "cancelled":
                    self.pyDownApp.updateDownloadList(key, ("last_update", "cancelled"))
                    self.updateCancelledDownload(key)
            else: self.printd("caiu no else")

    def updateQueueDownload(self, bot_file_id, data):
        self.printd(">>> Na fila:")
        chat_id = bot_file_id.split("_")[0]
        message_id = bot_file_id.split("_")[1]
        button_callback = self.CANCEL_DW_CALLBACK + ":" + bot_file_id
        # message = self.bot.get_messages(chat_id=int(chat_id), message_ids=int(message_id))
        # self.printd(message)
        self.bot.edit_message_text(
            chat_id=int(chat_id), 
            message_id=int(message_id),
            text="**Baixando:** {}".format(data["file_name"]),
            reply_markup=InlineKeyboardMarkup(
                [[InlineKeyboardButton("Cancelar ({})".format("Na fila"), bytes(button_callback, 'utf-8'))]]
            )
        )
    
    def updateProgressCount(self, bot_file_id, newData):
        self.printd(">>> Progresso:", newData)
        chat_id = bot_file_id.split("_")[0]
        message_id = bot_file_id.split("_")[1]
        button_callback = self.CANCEL_DW_CALLBACK + ":" + bot_file_id
        # message = self.bot.get_messages(chat_id=int(chat_id), message_ids=int(message_id))
        # self.printd(message)
        self.bot.edit_message_text(
            chat_id=int(chat_id), 
            message_id=int(message_id),
            text="**Baixando:** {}".format(newData["file_name"]),
            reply_markup=InlineKeyboardMarkup(
                [[InlineKeyboardButton("Cancelar ({}%)".format(newData["progress_count"]), bytes(button_callback, 'utf-8'))]]
            )
        )
    
    def updateCompleteDownload(self, bot_file_id):
        self.printd(">>> Complete:", bot_file_id)
        chat_id = bot_file_id.split("_")[0]
        message_id = bot_file_id.split("_")[1]
        # message = self.bot.get_messages(chat_id=int(chat_id), message_ids=int(message_id))
        # self.printd(message)
        self.bot.edit_message_text(
            chat_id=int(chat_id), 
            message_id=int(message_id),
            text="Concluído"
        )

    def updateCancelledDownload(self, bot_file_id):
        self.printd(">>> Cancelado:", bot_file_id)
        chat_id = bot_file_id.split("_")[0]
        message_id = bot_file_id.split("_")[1]
        # message = self.bot.get_messages(chat_id=int(chat_id), message_ids=int(message_id))
        # self.printd(message)
        self.bot.edit_message_text(
            chat_id=int(chat_id), 
            message_id=int(message_id),
            text="**Cancelado!**"
        )

    def run(self):
        self.printd("PyDownBot run")
        self.bot.run()

    def stop(self):
        return self.bot.stop()