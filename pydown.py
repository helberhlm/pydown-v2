import sys
import math
import signal
import threading
import time
import os
from dotenv import load_dotenv
from PyDownBot import PyDownBot
from PyDownUser import PyDownUser

class PyDownApp:

    DEBUG = True
    download_list = {}

    def __init__(self):
        load_dotenv()
        self.bot = PyDownBot(self)
        self.user = PyDownUser(self)
        self.running = False


    def printd(self, *args, sep=" ", end="\n", flush = True):
        if(self.DEBUG):
            print(*args, sep = sep, end = end, flush = flush)

    def updateDownloadList(self, bot_file_id, *values):
        """
        "<BOT_FILE_ID>": {
            "user_file_id": "id_to_userbot_identify_message_source",
            "file_name":"name_of_file_to_show_to_user"
            "path_download":"complete_path_to_download_with_filename",
            "download_status": "queue_downloading_complete_cancelled",
            "progress_count": "current_percentage_of_file_downloaded",
            "cancel_request": "flag_to_inform_download_cancel_request",
            "last_update":"save_last_update_in_message_on_user_chat"
        }
        """
        for item in values:
            if not bot_file_id in self.download_list:
                self.download_list[bot_file_id] = {}
            self.download_list[bot_file_id][item[0]] = item[1]
    def getDownloadItem(self, bot_file_id):
        return self.download_list[bot_file_id]

    def start(self):
        # Executar em Threads
        thread_user = threading.Thread(target=self.th_user)
        thread_user.start()

        thread_bot = threading.Thread(target=self.th_bot)
        thread_bot.start()
        self.running = True
        
    def notifyBotToNews(self):
        self.bot.onUpdateDownloadList(self.download_list.copy())

    def th_user(self):
        self.user.run()

    def th_bot(self):
        self.bot.run()

def finally_app(self):
        print("Stopping clients...")
        app.running = False
        app.bot.stop()
        app.user.stop()
        print("Clients stopped!")

signal.signal(signal.SIGTERM, finally_app)
app = PyDownApp()
app.start()
try:
    while(app.running):
        time.sleep(0.5)
except (KeyboardInterrupt):
    print("Finalizando serviços...")
    app.bot.stop()
    app.user.stop()
    print("Serviços finalizados!")
