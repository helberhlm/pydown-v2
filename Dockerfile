FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./
COPY PyDownBot/__init__.py PyDownBot/PyDownBot.py PyDownBot/
COPY PyDownUser/__init__.py PyDownUser/PyDownUser.py PyDownUser/
COPY pydown.py docker-entrypoint.sh  ./

RUN pip install --no-cache-dir -r requirements.txt
RUN chmod u+x docker-entrypoint.sh

VOLUME ["/usr/src/app/store/"]
ENTRYPOINT ["./docker-entrypoint.sh"]
