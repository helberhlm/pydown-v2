from pyrogram import Client
from pyrogram import Filters, MessageHandler
from flask import request
from dotenv import load_dotenv
import sys, os

class PyDownUser:
    DEBUG = "False"
    session_name = "my_account"
    bot_name = "pydown"
    api_id = 123456
    api_hash = "abcdef"

    CANCEL_DW_CALLBACK = "cancel_dw"

    def __init__(self, pyDownApp):
        self.loadConfig()
        self.pyDownApp = pyDownApp
        print(self.session_name, self.bot_name, self.api_hash, self.api_id)
        
        if(self.session_name == ":memory:"):
            with Client(
                self.session_name, 
                api_id=self.api_id,
                api_hash=self.api_hash,
                workdir="/usr/src/app/store/") as app:
                print("session_name >>>\n", app.export_session_string(), "\n<<<")
                exit()
        else:
            self.user = Client(
                self.session_name, 
                api_id=self.api_id,
                api_hash=self.api_hash,
                workdir="/usr/src/app/store/")
        self.initUserHandlers()

    def initUserHandlers(self):
        newMessage = MessageHandler(self.newMessage, filters=Filters.chat(self.bot_name))
        self.user.add_handler(newMessage)

    def printd(self, *args, sep=" ", end="\n", flush = True):
        if(self.DEBUG):
            print(*args, sep = sep, end = end, flush = flush)

    def loadConfig(self):        
        print("Lendo configurações...")
        ## Lê as configurações a partir do arquivo .env
        load_dotenv()
        self.api_id       = os.getenv('api_id', default=self.api_id)
        self.api_hash     = os.getenv('api_hash', default=self.api_hash)
        self.session_name = os.getenv('session_name', default=self.session_name)
        self.bot_name     = os.getenv('bot_name', default=self.bot_name)
        self.DEBUG        = os.getenv('debug', default=self.DEBUG).lower() == "true"

    def newMessage(self, client, message):
        # self.printd("New message:", message)
        if (message.reply_markup and
            message.reply_markup.inline_keyboard):
            callback_data = message.reply_markup.inline_keyboard[0][0]["callback_data"]
            if callback_data.split(':')[0] == self.CANCEL_DW_CALLBACK:
                bot_file_id = callback_data.split(':')[1]
                self.downloadFile(client, message, bot_file_id)
            
    def downloadFile(self, client, message, bot_file_id):
        args = (message, bot_file_id, client)
        message.reply_to_message.download(
            file_name=self.pyDownApp.getDownloadItem(bot_file_id)["path_download"],
            block=False,
            progress=self.progress_callback,
            progress_args=args
        )
        self.pyDownApp.updateDownloadList(bot_file_id, ("download_status", "queue"))
        print("Baixando...")

    def progress_callback(self, current, total, *args):
        # message = args[0]
        bot_file_id = args[1]
        client = args[2]
        progressCount = round((current/total)*100, 2)

        if(current == total):
            self.pyDownApp.updateDownloadList(bot_file_id, ("download_status", "complete"))
            status = str("Baixou " + str(bot_file_id) + " ("+ str(progressCount) + "%)")
        else:
            self.pyDownApp.updateDownloadList(bot_file_id, ("download_status", "downloading"))
            status = str("Baixando " + str(bot_file_id) + " ("+ str(progressCount) + "%)")

        self.pyDownApp.updateDownloadList(bot_file_id, ("progress_count", str(progressCount)))
        self.printd(status)
        dw_info = self.pyDownApp.getDownloadItem(bot_file_id)
        cancel_request = dw_info["cancel_request"] if "cancel_request" in dw_info else False
        if(cancel_request):
            self.printd("Cancelando {}...".format(bot_file_id))
            self.pyDownApp.updateDownloadList(bot_file_id, ("download_status", "cancelled"))
            self.pyDownApp.notifyBotToNews()
            client.stop_transmission() # the callback stop here
        self.pyDownApp.notifyBotToNews()


    def run(self):
        self.printd("PyDownUser run")
        self.user.run()

    def stop(self):
        return self.user.stop()

