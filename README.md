# PyDown-v2

### Pre-Requisites:
You will need `Docker` and `Docker Compose CLI` installed in your system.

### Instructions:
1. Clone the project


	`$ git clone https://gitlab.com/helberhlm/pydown-v2.git`

2. Get your Telegram API ID and API Hash


	https://my.telegram.org/apps

3. Keep in top level folder and create a `docker-compose.yml` file:


	`$ touch docker-compose.yml`


	`$ nano docker-compose.yml`

4. Now paste the YAML example below and replace the `volumes` and `command` sections values by your own:


    > Docker Compose example file
    ```yaml
    version: '3.7'
    services:
      pydown-v2:
        container_name: pydown-v2
        restart: always
        network_mode: host
        build:
          context: ./pydown-v2/
          dockerfile: Dockerfile
          image: pydown-v2
        volumes:
          # Change only before colon ":" with your host path to persist datas
          - "~/Documents/Apps-Docker/pydown-v2/store:/usr/src/app/store"
          - "~/Documents/Apps-Docker/pydown-v2/data:/usr/src/app/data"
        command: [
          # Get your own Telegram API key from https://my.telegram.org/apps
          "--api_id", "123456",
          "--api_hash", "0123456789abcdef0123456789abcdef",
          # Set ":memory:" to store session in one string or set a custom name to store session in *.session file
          # If set :memory:, the string session generated must be placed here!
          "--session_name", "my_session",
          # Your username from Telegram client account (will be removed in future)
          "--my_username", "myUsername",
          # Bot name and token created from @BotFather
          "--bot_name", "mybotname",
          "--bot_token", "123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11",
          # Set to True to see outputs
          "--debug", "false",
          # Username of Telegram that will download the files sent by the bot (it's same of my_username)
          "--user_downloader", "myUsername",
          # List of username that the bot will respond (separate users by comma: "userA, userB")
          "--allowed_users", "myUsername,usernameA,usernameB",
          # List of Names and Paths to show in bot chat buttons (ButtonName:data/pathToDownload)
          "--path", "Temp:data/temp/",
          "--path", "Movies:data/movies/",
          "--path", "Series:data/series/",
          "--path", "Others:data/others/",
        ]
        # Optional
        logging:
          driver: "none"

    ```

5. Verify for errors:


	`docker-compose config`


    > It's should print the docker-compose file on screen


6. Build the image:


	`docker-compose build`


7. First run:


    For first run, you need make Telegram login by console, for do this, enter the command:
	
	`docker-compose run pydown-v2`
	
    > You will see a `Thread-2` like error, it's normal. Enter the requested data... And then press `CTRL+C` to stop session


8. Execute the bot:


    `docker-compose up -d`

### Development:
1. Install


    `pip install virtualenv`
2. Create


    `virtualenv pydown`
3. Activate


    ` .\pydown\Scripts\activate`
4. Install requirements


    `pip install -r requirements.txt`
5. Exit Python Env


    ` .\pydown\Scripts\deactivate`

https://pandao.github.io/editor.md/en.html
